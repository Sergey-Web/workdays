<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Workday;
use App\Pause;

class UserController extends Controller
{

    public function index()
    {
        if(Auth::user()->role == 1) {
            return redirect('admin');
        }

        return view('user');
    }

}
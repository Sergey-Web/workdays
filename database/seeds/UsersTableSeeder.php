<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['login'=>'admin','name' => 'admin', 'email' => 'admin@gmail.com', 'password' => bcrypt('secret'), 'role' => 1],
            ['login'=>'Sergey','name' => 'Sergey', 'email' => 'Sergey@gmail.com', 'password' => bcrypt('secret'), 'role' => 0],
            ['login'=>'Rubbio','name' => 'Rubbio', 'email' => 'Rubbio@gmail.com', 'password' => bcrypt('secret'), 'role' => 0],
            ['login'=>'Gnom','name' => 'Gnom', 'email' => 'Gnom@gmail.com', 'password' => bcrypt('secret'), 'role' => 0],

        ]);

        DB::table('workdays')->insert([
            ['date' => '2016-11-16', 'start' => '07:10:10', 'end' => null, 'break' => '01:01:10', 'user_id' => 1],
            ['date' => '2016-11-16', 'start' => '07:12:10', 'end' => null, 'break' => '00:50:10', 'user_id' => 2],
            ['date' => '2016-11-16', 'start' => '07:50:10', 'end' => null, 'break' => '01:20:10', 'user_id' => 3],
        ]);

        DB::table('pauses')->insert([
            ['date' => '2016-11-16', 'start' => '07:12:10', 'end'=> '09:10:25', 'comment' => 'Potomuchto est na svete...', 'user_id' => 1],
            ['date' => '2016-11-16', 'start' => '06:14:10', 'end'=> '06:30:30', 'comment' => 'Jizn bolj', 'user_id' => 2],
            ['date' => '2016-11-16', 'start' => '06:53:10', 'end'=> '07:10:35', 'comment' => 'Ras-dwa-tri', 'user_id' => 2],
        ]);
    }
}
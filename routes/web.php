<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect('user');
});


Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('admin', ['uses' => 'AdminController@index', 'as' => 'admin']);
    Route::get('user', ['uses' => 'UserController@index', 'as' => 'user']);
    Route::get('check-start', ['uses' => 'AjaxController@checkStart']);
    Route::get('start-workday', ['uses' => 'AjaxController@startWorkday']);
    Route::get('end-workday', ['uses' => 'AjaxController@endWorkday']);
    Route::get('start-pause', ['uses' => 'AjaxController@Pause']);
});
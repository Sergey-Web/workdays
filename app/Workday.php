<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workday extends Model
{
    protected $primaryKey = 'id';
    public $incrementing = TRUE;
    public $timestamps = FALSE;

    protected $fillable = ['date','start','end','break','user_id'];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}

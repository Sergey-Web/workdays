var timer1 = null;
var timer2 = null;
var timer3 = null;

//timerRev workday
function CountdownTimer(elm, tl, mes) {
    this.initialize.apply(this, arguments);
}

CountdownTimer.prototype = {
    initialize: function (elm, tl, mes) {
        this.elem = document.getElementById(elm);
        this.tl = tl;
        this.mes = mes;
    }, countDown: function () {
        var timer1 = '';
        var today = new Date();
        var day = Math.floor((this.tl - today) / (24 * 60 * 60 * 1000));
        var hour = Math.floor(((this.tl - today) % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000));
        var min = Math.floor(((this.tl - today) % (24 * 60 * 60 * 1000)) / (60 * 1000)) % 60;
        var sec = Math.floor(((this.tl - today) % (24 * 60 * 60 * 1000)) / 1000) % 60 % 60;
        var me = this;

        if (( this.tl - today ) > 0) {
            timer1 += '<span class="number-wrapper">' +
                        '<div class="line"></div>' +
                        '<div class="caption">HOURS</div>' +
                        '<span class="number hour">' + this.addZero(hour) + '</span>' +
                     '</span>';
            timer1 += '<span class="number-wrapper">' +
                        '<div class="line"></div>' +
                        '<div class="caption">MINS</div><' +
                        'span class="number min">' + this.addZero(min) + '</span>' +
                     '</span>' +
                     '<span class="number-wrapper">' +
                        '<div class="line"></div>' +
                        '<div class="caption">SECS</div>' +
                        '<span class="number sec">' + this.addZero(sec) + '</span>' +
                     '</span>';
            if(this.elem) {
                this.elem.innerHTML = timer1;
                this.tid = setTimeout(function () {
                    me.countDown();
                }, 1000);
            }
        } else {
            this.elem.innerHTML = this.mes;
            return;
        }
    }, addZero: function (num) {
        return ('0' + num).slice(-2);
    }
}

function CDT(date, clear, play, pause) {
    if(clear) {
        clearTimeout(timer1.tid);
        clearInterval(timer2);
        clearInterval(timer3);
    }
    // Set countdown limit
    var tl = new Date(date+' 23:59:00');

    // You can add time's up message here
    timer1 = new CountdownTimer('timerRev', tl,
        '<span class="number-wrapper">' +
            '<div class="line"></div>' +
            '<div class="caption">HOURS</div>' +
            '<span class="number hour">00</span>' +
        '</span>' +
        '<span class="number-wrapper">' +
            '<div class="line"></div>' +
            '<div class="caption">MINS</div>' +
            '<span class="number min">00</span></span>' +
        '<span class="number-wrapper">' +
            '<div class="line"></div>' +
            '<div class="caption">SECS</div>' +
            '<span class="number sec">00</span>' +
        '</span>');
    timer1.countDown();
    if(play) {
        timer2 = setInterval(startTimer, 1000);
    }
    if(pause) {
        timer3 = setInterval(startPause, 1000);
    }
}
CDT('0000-00-00');

//timer workday
function startTimer() {
    var s = $('#timerWork>.number-wrapper>.sec').text();
    var m = $('#timerWork>.number-wrapper>.min').text();
    var h = $('#timerWork>.number-wrapper>.hour').text();

    s++;
    s = s < 10 ? "0" + s : s;


    if(s == 60) {
        s = "00";
        m++;
        m = m < 10 ? "0" + m : m;
        if(m == 60) {
            m = "00";
            h++;
            if(h == 24) {
                h = "00";
                h = h < 10 ? "0"+h : h;
            }
        }
    }

    $('#timerWork>.number-wrapper>.hour').text(h);
    $('#timerWork>.number-wrapper>.min').text(m);
    $('#timerWork>.number-wrapper>.sec').text(s);
}

//timer pause
function startPause() {
    var s = $('#timerPause>.number-wrapper>.sec').text();
    var m = $('#timerPause>.number-wrapper>.min').text();
    var h = $('#timerPause>.number-wrapper>.hour').text();

    s++;
    s = s < 10 ? "0" + s : s;

    if(s == 60) {
        s = "00";
        m++;
        m = m < 10 ? "0" + m : m;
        if(m == 60) {
            m = "00";
            h++;
            h = h < 10 ? "0"+h : h;
            if(h == 24) {
                h = "00";
            }
        }
    }

    $('#timerPause>.number-wrapper>.hour').text(h);
    $('#timerPause>.number-wrapper>.min').text(m);
    $('#timerPause>.number-wrapper>.sec').text(s);
}


$.ajax({
    type: "GET",
    url: "check-start",
    success: function(res) {
        if(res != 'false') {
            if(res.time.end == null) {
                if(res.pause == 'off') {
                    CDT(res.time.now_date, false, true, false);
                    $('#timerWork>.number-wrapper>.hour').text(res.timer.hour);
                    $('#timerWork>.number-wrapper>.min').text(res.timer.min);
                    $('#timerWork>.number-wrapper>.sec').text(res.timer.sec);

                    $('#timerPause>.number-wrapper>.hour').text(res.pause_time.hour);
                    $('#timerPause>.number-wrapper>.min').text(res.pause_time.min);
                    $('#timerPause>.number-wrapper>.sec').text(res.pause_time.sec);
                } else {
                    CDT(res.time.now_date, false, false, true);
                    $('#timerWork>.number-wrapper>.hour').text(res.timer.hour);
                    $('#timerWork>.number-wrapper>.min').text(res.timer.min);
                    $('#timerWork>.number-wrapper>.sec').text(res.timer.sec);

                    $('#timerPause>.number-wrapper>.hour').text(res.pause_time.hour);
                    $('#timerPause>.number-wrapper>.min').text(res.pause_time.min);
                    $('#timerPause>.number-wrapper>.sec').text(res.pause_time.sec);
                }
            } else {
                $('#timerWork>.number-wrapper>.hour').text(res.timer.hour);
                $('#timerWork>.number-wrapper>.min').text(res.timer.min);
                $('#timerWork>.number-wrapper>.sec').text(res.timer.sec);

                $('#timerPause>.number-wrapper>.hour').text(res.pause_time.hour);
                $('#timerPause>.number-wrapper>.min').text(res.pause_time.min);
                $('#timerPause>.number-wrapper>.sec').text(res.pause_time.sec);
            }
        }
    }
});

$('#startTimer').on('click',function() {
    $.ajax({
        type: "GET",
        url: "start-workday",
        success: function(res) {
            if(res != 'false') {
                CDT(res.now_date, false, true);
                $('#timerWork>.number-wrapper>.hour').text(res.hour);
                $('#timerWork>.number-wrapper>.min').text(res.min);
                $('#timerWork>.number-wrapper>.sec').text(res.sec);
            }
        }
    })
});

$('#endTimer').on('click',function() {
    $.ajax({
        type: "GET",
        url: "end-workday",
        success: function(res) {
            if(res != 'false') {
                CDT('0000-00-00', true);
            }
        }
    });
});

$('#pauseTimer').on('click',function(){
    $.ajax({
        type: "GET",
        url: "start-pause",
        success: function(res) {
            if(res != 'false') {
                if(res.pause.start) {
                    CDT(res.date, true, false, true);
                    $('#timerPause>.number-wrapper>.hour').text('00');
                    $('#timerPause>.number-wrapper>.min').text('00');
                    $('#timerPause>.number-wrapper>.sec').text('00');

                } else {
                    CDT(res.date, true, true, false);
                    $('#timerWork>.number-wrapper>.hour').text(res.timer.hour);
                    $('#timerWork>.number-wrapper>.min').text(res.timer.min);
                    $('#timerWork>.number-wrapper>.sec').text(res.timer.sec);

                    $('#timerPause>.number-wrapper>.hour').text(res.pause.hour);
                    $('#timerPause>.number-wrapper>.min').text(res.pause.min);
                    $('#timerPause>.number-wrapper>.sec').text(res.pause.sec);
                }
            }
        }
    });
});

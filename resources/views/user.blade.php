@extends('layouts.app')

@section('navbar-right')
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            {{ Auth::user()->name }} <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    Logout
                </a>

                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </li>
@endsection

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                <div class="wrap-timers">
                    <div class="timer__end-workday">
                        <h3>End working day:</h3>
                        <div id="timerRev"></div>
                    </div>
                    <div class="timer__working-time">
                        <h3>Working time:</h3>
                        <div id="timerWork">
                            <span class="number-wrapper">
                                <div class="line"></div>
                                <div class="caption">HOURS</div>
                            <span class="number hour">00</span></span>
                            <span class="number-wrapper">
                                <div class="line"></div>
                                <div class="caption">MINS</div>
                            <span class="number min">00</span></span>
                            <span class="number-wrapper">
                                <div class="line"></div>
                                <div class="caption">SECS</div>
                            <span class="number sec">00</span></span>
                        </div>
                    </div>
                    <div class="timer__pause">
                        <h3>Pause:</h3>
                        <div id="timerPause">
                            <span class="number-wrapper">
                                <div class="line"></div>
                                <div class="caption">HOURS</div>
                            <span class="number hour">00</span></span>
                            <span class="number-wrapper">
                                <div class="line"></div>
                                <div class="caption">MINS</div>
                            <span class="number min">00</span></span>
                            <span class="number-wrapper">
                                <div class="line"></div>
                                <div class="caption">SECS</div>
                            <span class="number sec">00</span></span>
                        </div>
                    </div>
                </div>
                <button class="btn btn-success" id="startTimer">Start workday</button>
                <button class="btn btn-warning" id="pauseTimer">Pause</button>
                <button class="btn btn-danger" id="endTimer">End workday</button>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    @parent
    <script src="{{asset('js/countdown.js')}}"></script>
@endsection


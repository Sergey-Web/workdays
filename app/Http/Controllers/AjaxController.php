<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Workday;
use App\Pause;

class AjaxController extends Controller
{

    public function checkStart()
    {
        $data = $this->getUser();
        $arr_pauses = $this->checkPause();

        if(!$data) {
            return 'false';
        }

        if(!$arr_pauses == false) {
            foreach($arr_pauses as $item_pause) {
                $arr_pause[] = $item_pause;
            }
        } else {
            $arr_pause = false;
        }

        if($arr_pause === false){

            if($data['end'] == null) {
                $end_time = date_create(date('H:i:s'));
            }

            $start_time = date_create($data['start']);
            $end_time = date_create($data['end']);
            $interval = $start_time->diff($end_time);

            $time['hour'] = $interval->h < 10 ? '0'.$interval->h : $interval->h;
            $time['min'] = $interval->i < 10 ? '0'.$interval->i : $interval->i;
            $time['sec'] = $interval->s < 10 ? '0'.$interval->s : $interval->s;

            return ['time'=>$data,'timer'=>$time, 'pause_time'=>false, 'pause'=>'off'];
        } else {
            if(end($arr_pause)['end'] == null) {
                $pause_hour = 0;
                $pause_min = 0;
                $pause_sec = 0;
                foreach($arr_pause as $item_pause) {
                    $start_pauses = date_create(($item_pause)['start']);
                    $end_pauses = date_create(($item_pause)['end']);
                    $interval_pauses = $start_pauses->diff($end_pauses);

                    $pause_hour += $interval_pauses->h;
                    $pause_min += $interval_pauses->i;
                    $pause_sec += $interval_pauses->s;
                }

                $pauses['hour'] = date('H', mktime($pause_hour,$pause_min,$pause_sec,0,0,0));
                $pauses['min'] = date('i', mktime($pause_hour,$pause_min,$pause_sec,0,0,0));
                $pauses['sec'] = date('s', mktime($pause_hour,$pause_min,$pause_sec,0,0,0));

                $start_time = date_create($data['start']);
                $end_time = date_create($data['end']);
                $interval = $start_time->diff($end_time);

                $sum_time_pause = mktime(
                    $interval->h - $pauses['hour'],
                    $interval->i - $pauses['min'],
                    $interval->s - $pauses['sec'],
                    0,0,0);

                $time['hour'] = date('H', $sum_time_pause);
                $time['min'] = date('i', $sum_time_pause);
                $time['sec'] = date('s', $sum_time_pause);

                $start_pause = date_create(end($arr_pause)['start']);
                $end_pause = date_create(date('H:i:s'));
                $interval_time_pause = $start_pause->diff($end_pause);

                $pause_now['hour'] = $interval_time_pause->h < 10 ? '0'.$interval_time_pause->h : $interval_time_pause->h;
                $pause_now['min'] = $interval_time_pause->i < 10 ? '0'.$interval_time_pause->i : $interval_time_pause->i;
                $pause_now['sec'] = $interval_time_pause->s < 10 ? '0'.$interval_time_pause->s : $interval_time_pause->s;

                return ['time'=>$data,'timer'=>$time,'pause_time'=>$pause_now, 'pause'=>'on'];
            } else {
                $pause_hour = 0;
                $pause_min = 0;
                $pause_sec = 0;
                foreach($arr_pause as $item_pause) {
                    $start_pauses = date_create(($item_pause)['start']);
                    $end_pauses = date_create(($item_pause)['end']);
                    $interval_pauses = $start_pauses->diff($end_pauses);

                    $pause_hour += $interval_pauses->h;
                    $pause_min += $interval_pauses->i;
                    $pause_sec += $interval_pauses->s;
                }

                $pauses['hour'] = date('H', mktime($pause_hour,$pause_min,$pause_sec,0,0,0));
                $pauses['min'] = date('i', mktime($pause_hour,$pause_min,$pause_sec,0,0,0));
                $pauses['sec'] = date('s', mktime($pause_hour,$pause_min,$pause_sec,0,0,0));

                $start_time = date_create($data['start']);
                $end_time = date_create($data['end']);
                $interval = $start_time->diff($end_time);

                $sum_time_pause = mktime(
                    $interval->h - $pauses['hour'],
                    $interval->i - $pauses['min'],
                    $interval->s - $pauses['sec'],
                    0,0,0);

                $time['hour'] = date('H', $sum_time_pause);
                $time['min'] = date('i', $sum_time_pause);
                $time['sec'] = date('s', $sum_time_pause);

                return ['time'=>$data, 'timer'=>$time, 'pause_time'=>$pauses, 'pause'=>'off'];
            }
        }
    }

    public function startWorkday()
    {
        $check = $this->checkStartWorkday();
        if ($check === true) {
            $add_start = Workday::create([
                'date' => date('Y-m-d'),
                'start' => date('H:i:s'),
                'user_id' => Auth::id()
            ]);

            $start_time = date_create($add_start->start);
            $end_time = date_create(date('H:i:s'));
            $interval = $start_time->diff($end_time);

            $data['hour'] = $interval->h < 10 ? '0'.$interval->h : $interval->h;
            $data['min'] = $interval->i < 10 ? '0'.$interval->i : $interval->i;
            $data['sec'] = $interval->s < 10 ? '0'.$interval->s : $interval->s;

            $data['now_date'] = $add_start->date;

            return $data;
        }

        return 'false';
    }

    public function endWorkday()
    {
        $check = $this->checkEndWorkday();
        if($check != false) {
            foreach($check as $item){
                $item->end = date('H:i:s');
            }
            $item->save();
            return date('Y-m-d');
        }

        return 'false';

    }

    public function Pause()
    {
        $data = $this->getUser();


        if($data['end'] == null) {
            $end_time = date_create(date('H:i:s'));
        } else {
            return 'false';
        }

        $start_time = date_create($data['start']);
        $end_time = date_create($data['end']);
        $interval = $start_time->diff($end_time);

        $time['hour'] = $interval->h < 10 ? '0'.$interval->h : $interval->h;
        $time['min'] = $interval->i < 10 ? '0'.$interval->i : $interval->i;
        $time['sec'] = $interval->s < 10 ? '0'.$interval->s : $interval->s;

        $arr_pauses = $this->checkPause();

        $start_pause = false;

        if($arr_pauses == false) {
            $start_pause = false;
        } else {
            foreach($arr_pauses as $item_pause) {
                if($item_pause['end'] == null) {
                    $start_pause = true;
                }
            }
        }

        if($start_pause == false) {
            $pause['start'] = $this->startPause();
            $time = false;
        } else {
            $this->endPause();
            $pause['end'] = date('Y-m-d');

            $pause_hour = 0;
            $pause_min = 0;
            $pause_sec = 0;
            foreach($arr_pauses as $item_pause) {
                $start_pauses = date_create(($item_pause)['start']);
                $end_pauses = date_create(($item_pause)['end']);
                $interval_pauses = $start_pauses->diff($end_pauses);

                $pause_hour += $interval_pauses->h;
                $pause_min += $interval_pauses->i;
                $pause_sec += $interval_pauses->s;
            }

            $pause['hour'] = date('H', mktime($pause_hour,$pause_min,$pause_sec,0,0,0));
            $pause['min'] = date('i', mktime($pause_hour,$pause_min,$pause_sec,0,0,0));
            $pause['sec'] = date('s', mktime($pause_hour,$pause_min,$pause_sec,0,0,0));;

            $sum_time_pause = mktime(
                $interval->h - $pause['hour'],
                $interval->i - $pause['min'],
                $interval->s - $pause['sec'],
                0,0,0);

            $time['hour'] = date('H', $sum_time_pause);
            $time['min'] = date('i', $sum_time_pause);
            $time['sec'] = date('s', $sum_time_pause);
        }

        return ['date'=>date('Y-m-d'), 'timer'=>$time,'pause'=>$pause];
    }

    protected function checkStartWorkday()
    {
        $check = Workday::select('date','user_id', 'id')
            ->where('user_id', Auth::id())
            ->where('date', date('Y-m-d'))
            ->get();

        if(count($check) >= 1) {
            return false;
        }

        return true;
    }

    protected function checkEndWorkday()
    {
        $check = Workday::select('id','date','end','user_id')
            ->where('user_id', Auth::id())
            ->where('date', date('Y-m-d'))
            ->where('end', null)->get();

        if(count($check) != 1) {
            return false;
        }

        return $check;
    }

    protected function checkPause()
    {
        $check = Pause::select('id','date','start','end','user_id')
            ->where('user_id', Auth::id())
            ->where('date', date('Y-m-d'))
            ->get();
        if(count($check) == 0) {
            return false;
        }

        return $check;
    }

    protected function getUser()
    {
        $users = User::select('id')
            ->where('id',Auth::id())
            ->get();

        foreach($users as $user) {
            $workdays = $user->workdays->where('date',date('Y-m-d'));
            if(count($workdays) == 0) {
                return false;
            } else {
                foreach($workdays as $workday){
                    $data['now_date'] = $workday->date;
                    $data['start'] = $workday->start;
                    $data['end'] = $workday->end;
                }
            }
        }

        return $data;
    }

    protected function startPause()
    {
        $create_pause = Pause::create([
                'date' => date('Y-m-d'),
                'start' => date('H:i:s'),
                'user_id' => Auth::id()
            ]);
        return $create_pause;
    }

    protected function endPause()
    {
        $check = $this->checkPause();

        foreach($check as $arr_pause) {
            if($arr_pause->end == null) {
                $arr_pause->end = date('H:i:s');
            }
        }
        return $arr_pause->save();
    }
}
